#include "MeshGateway.h"
#include "Payloads.h"
#include <iostream>

namespace nrfmesh
{
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::FunctionTemplate;
using v8::Isolate;
using v8::Local;
using v8::Object;
using v8::String;
using v8::Value;
using v8::Handle;
using v8::TryCatch;
using v8::Number;
using v8::Null;
using v8::Persistent;
using v8::Integer;
using v8::Number;
using v8::Context;

struct Work
{
    uv_work_t request;
    uv_async_t async;
    Persistent<Function> callback;
    void *message;
    int result;
    RF24Network *network;
    RF24Mesh *mesh;
    RF24 *radio;
};

Persistent<Function> MeshGateway::constructor;
MeshGateway::MeshGateway(uint8_t cepin, uint8_t cspin, uint32_t spiSpeed)
{
    radio = new RF24(cepin, cspin, spiSpeed);
    network = new RF24Network(*radio);
    mesh = new RF24Mesh(*radio, *network);
    mesh->setNodeID(0);
    mesh->begin();
}

MeshGateway::~MeshGateway()
{
    delete radio;
    delete network;
    delete mesh;
}

void MeshGateway::Init(Local<Object> exports)
{
    Isolate *isolate = exports->GetIsolate();

    Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);
    tpl->SetClassName(String::NewFromUtf8(isolate, "MeshGateway"));
    tpl->InstanceTemplate()->SetInternalFieldCount(1);

    NODE_SET_PROTOTYPE_METHOD(tpl, "listen", Listen);
    NODE_SET_PROTOTYPE_METHOD(tpl, "send", Send);
    NODE_SET_PROTOTYPE_METHOD(tpl, "printDetails", PrintDetails);

   constructor.Reset(isolate, tpl->GetFunction());
   exports->Set(String::NewFromUtf8(isolate, "MeshGateway"),
               tpl->GetFunction());
}

void MeshGateway::PrintDetails(const v8::FunctionCallbackInfo<v8::Value> &args)
{
  MeshGateway *obj = ObjectWrap::Unwrap<MeshGateway>(args.Holder());
  obj->radio->printDetails();
}


void MeshGateway::New(const FunctionCallbackInfo<Value> &args)
{
    if (args.IsConstructCall())
    {
        uint8_t cepin = args[0]->IsUndefined() ? 0 : args[0]->NumberValue();
        uint8_t cspin = args[1]->IsUndefined() ? 0 : args[1]->NumberValue();
        uint32_t spiSpeed = args[2]->IsUndefined() ? 0 : args[2]->NumberValue();

        MeshGateway *obj = new MeshGateway(cepin, cspin, spiSpeed);

        obj->Wrap(args.This());
        args.GetReturnValue().Set(args.This());
    }
}

void MeshGateway::Listen(const FunctionCallbackInfo<Value> &args)
{
    Isolate *isolate = args.GetIsolate();

    Work *work = new Work();

    work->request.data = work;
    work->async.data = work;

    MeshGateway *obj = ObjectWrap::Unwrap<MeshGateway>(args.Holder());

    work->radio = obj->radio;
    work->network = obj->network;
    work->mesh = obj->mesh;
    

    Handle<Function> callback = Handle<Function>::Cast(args[0]);
    work->callback.Reset(isolate, callback);
    uv_loop_t* loop = uv_default_loop();
    uv_async_init(loop, &work->async, ListenAsyncRecivedData);
    uv_queue_work(loop, &work->request, ListenAsync, ListenAsyncComplete);
    uv_run(loop, UV_RUN_DEFAULT);
}

void MeshGateway::Send(const FunctionCallbackInfo<Value> &args)
{
    MeshGateway *obj = ObjectWrap::Unwrap<MeshGateway>(args.Holder());
    int m = 1;
    obj->mesh->write(&m, 'M', sizeof(int), 1);
}

void MeshGateway::ListenAsync(uv_work_t *req)
{
    Work *work = static_cast<Work *>(req->data);
    while (1)
    {
        work->mesh->update();
        work->mesh->DHCP();

        if (work->network->available())
        {
            std::cout << 1;
            uv_async_send(&work->async);
        }

        delay(2);
    }
}
void MeshGateway::ListenAsyncComplete(uv_work_t *req, int status) {

}


void MeshGateway::ListenAsyncRecivedData(uv_async_t *req)
{
    Isolate *isolate = Isolate::GetCurrent();

    v8::HandleScope handleScope(isolate);

    Local<Object> returnObject = Object::New(isolate);

    Work *work = static_cast<Work *>(req->data);

    RF24NetworkHeader header;
    work->network->peek(header);

    switch (header.type)
    {
    case 'T':
    {
        TemperaturePayload *temperature = new TemperaturePayload();
        work->network->read(header, temperature, sizeof(TemperaturePayload));
        AddTemp(temperature, returnObject, isolate);
    }
    }

    AddNetworkInfo(header, work->mesh->getNodeID(header.from_node), returnObject, isolate);

    Handle<Value> argv[] = {returnObject};

    Local<Function>::New(isolate, work->callback)->Call(isolate->GetCurrentContext()->Global(), 1, argv);

    // uv_queue_work(uv_default_loop(), &work->request, ListenAsync, ListenAsyncComplete);
}

void MeshGateway::AddTemp(TemperaturePayload *temp, Local<Object> obj, Isolate *isolate)
{
    obj->Set(String::NewFromUtf8(isolate, "temperature"), Number::New(isolate, temp->temperature));
}

void MeshGateway::AddNetworkInfo( RF24NetworkHeader header, int from_node, Local<Object> obj, Isolate *isolate)
{
    uint16_t type = (uint16_t)header.type;
    obj->Set(String::NewFromUtf8(isolate, "messageId"), Number::New(isolate, header.id));
    obj->Set(String::NewFromUtf8(isolate, "fromNode"), Number::New(isolate, from_node));
    obj->Set(String::NewFromUtf8(isolate, "messageType"), String::NewFromTwoByte(isolate, &type));
}

}