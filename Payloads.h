#ifndef PAYLOADS_H
#define PAYLOADS_H

namespace nrfmesh
{
struct TemperaturePayload
{
    float temperature;
};
}

#endif