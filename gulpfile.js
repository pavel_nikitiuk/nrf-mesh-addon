'use strict'

var gulp = require('gulp');
var scp = require('gulp-scp2');
var watch = require('gulp-watch');

var scpSettings = {
    host: 'raspberrypi.local',
    username: 'pi',
    password: 'raspberry',
    dest: '/home/pi/nrf-gateway',
    watch: function(client) {
      client.on('write', function(o) {
        console.log('write %s', o.destination);
      });
    }
};

gulp.task('watch', function () {
    watch(['**/*.*','**/**' , '!node_modules/**'])
        .pipe(scp(scpSettings))
        .on('error', function (err) {
            console.log(err);
        });
});

gulp.task('default', function () {
    gulp.src(['**/*.*','**/**', '!node_modules/**'])
        .pipe(scp(scpSettings))
        .on('error', function (err) {
            console.log(err);
        });
});