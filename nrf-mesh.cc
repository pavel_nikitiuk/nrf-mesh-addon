#include <node.h>
#include "MeshGateway.h"

namespace nrfmesh {

using v8::Local;
using v8::Object;

void InitAll(Local<Object> exports) {
  MeshGateway::Init(exports);
}

NODE_MODULE(addon, InitAll)

}  