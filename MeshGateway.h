#ifndef MESH_GAYEWAY_H
#define MESH_GAYEWAY_H

#include <node.h>
#include <node_object_wrap.h>
#include <RF24/RF24.h>
#include <RF24Network/RF24Network.h>
#include <RF24Mesh/RF24Mesh.h>
#include <uv.h>
#include "Payloads.h"

namespace nrfmesh
{

class MeshGateway : public node::ObjectWrap
{
  public:
    static void Init(v8::Local<v8::Object> exports);

  private:
    explicit MeshGateway(uint8_t cepin, uint8_t cspin, uint32_t spiSpeed);
    ~MeshGateway();
    static v8::Persistent<v8::Function> constructor;
    static void New(const v8::FunctionCallbackInfo<v8::Value> &args);
    static void PrintDetails(const v8::FunctionCallbackInfo<v8::Value> &args);
    static void Listen(const v8::FunctionCallbackInfo<v8::Value> &args);
    static void Send(const v8::FunctionCallbackInfo<v8::Value> &args);
    static void ListenAsync(uv_work_t *req);
    static void ListenAsyncComplete(uv_work_t *req, int status);
    static void ListenAsyncRecivedData(uv_async_t *req);
    static void AddTemp(TemperaturePayload *temp, v8::Local<v8::Object> obj, v8::Isolate *isolate);
    static void AddNetworkInfo(RF24NetworkHeader header, int from_node, v8::Local<v8::Object> obj, v8::Isolate *isolate);
    
    RF24* radio;
    RF24Network* network;
    RF24Mesh* mesh;
};

} // namespace demo

#endif