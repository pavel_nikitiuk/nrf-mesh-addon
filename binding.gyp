{
  "targets": [
    {
      "target_name": "addon",
      "include_dirs":["./NRF/", "libs"],
      "libraries":["-L/NRF/RF24/RF24.h", "-lrf24", "-lrf24network", "-lrf24mesh"],
      "sources": [ "nrf-mesh.cc",  "MeshGateway.cc"],
      "msvs_settings": {
        "VCCLCompilerTool": {
          "ExceptionHandling": 1
        }
      }
    }
  ]
}